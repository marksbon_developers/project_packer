<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Universal_Update extends CI_Model 
{
  /***********************************************
            Category
    ************************************************/
    
    public function OneFieldUpdate($tablename,$IdField,$FieldUpdate,$data) {

        $this->db->set($FieldUpdate,$data[$FieldUpdate]);
        
        $this->db->where($IdField,$data[$IdField]);
        
        $query = $this->db->update($tablename);
        
        $result = $this->db->affected_rows();
                
        return (($result > 0) ? TRUE : FALSE );

    }

  /***********************************************
    Single Field Update
  ************************************************/
  public function Multiple_Update($tablename,$data,$where_condition) 
  {
    $this->db->where($where_condition);
      
    $query = $this->db->update($tablename,$data);
      
    $result = $this->db->affected_rows();
              
    return (($result > 0) ? TRUE : FALSE );
  }

  /******************************** Data Insertion ******************************************/
    
    /***********************************************
        Last Invoice ID
    ************************************************/
    public function last_invoice_id_increase($lastid) 
    {
      $tablename = "general_last_ids";

      $this->db->set('invoice_no',$lastid);

      $this->db->where('id','1');
          
      $query = $this->db->update($tablename);

      $result = $this->db->affected_rows(); 
        
      return(($result > 0) ? true : false);
    }


    
}//End of class
