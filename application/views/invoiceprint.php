  <head>
  <title>Invoice</title>
  
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/vendor.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/flat-admin.css">

  <!-- Theme -->
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/blue-sky.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/blue.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/red.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>resources/css/theme/yellow.css">

</head>

<body onload="window.print()">
    <div class="wrapper">
      <!-- Main content -->
      <section class="invoice">
        <!-- title row -->
      <div class="row">
          <div class="col-xs-12">
          <div class="col-xs-4 invoice-col">
           <!-- <h2 class="page-header"> -->
            <img src="<?= base_url().@$_SESSION['company_logo'] ?>" style="margin-left:0em;width:150px;height:60px;" class="">
            </h2>
            <!--</div>--><hr></hr>
          </div>
            <div class="col-xs-4 invoice-col" style="margin-top:60px;"><hr></hr>  </div>
              <div class="col-xs-4 invoice-col">
                
            <address style="height:60px;">
              <strong><?= @$_SESSION['company_name'] ?></strong><br>
              <?= @$_SESSION['company_address'] ?><br>
              <?= @$_SESSION['company_location'] ?><br>
              <?= @$_SESSION['company_tel'] ?> / 
              <?= @$_SESSION['company_alttel'] ?><br>
           
      
            </address>
            <hr></hr>
          <!--<h2 class="page-header"></h2>-->
          </div><!-- /.col -->
          
          </div><!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
        
          <?php 
            $data = ['sup_id' => base64_decode($printdata['sup_id'])];
            $supplier = $this->Universal_Retrieval->ret_data_with_s_cond_row('suppliers_full_details','sup_id',$data);
          ?>
         
       
        </div><!-- /.row -->

        <!-- Table row -->
        <div class="row">
          <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Product</th>
                   <th>Quanity</th>
                  <th>Unit</th>
                  <th>Sub-Total</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  if(!empty($printdata)) :
                    $counter = 1;
                    
                    for($a = 0; $a < sizeof($printdata['products']); $a++) :
                ?>
                <tr>
                  <td><?= $counter ?></td>
                  <td><?= $printdata['products'][$a] ?></td>
                  <td><?= $printdata['quantity'][$a] ?></td>
                  <td><?= "GHȻ ".$printdata['prices'][$a] ?></td>
                  <td><?= "GHȻ ".$printdata['totals'][$a] ?></td>
                </tr>
                <?php
                      $counter++; 
                    endfor;
                  endif;
                ?>
              </tbody>
            </table>
          </div><!-- /.col -->
        </div><!-- /.row -->

        <div class="row">
          <!-- accepted payments column -->
          <div class="col-xs-6">
            <p class="lead">Remarks</p>
             <textarea style="width:300px; height:180px;border:0px;"></textarea>
          </div><!-- /.col -->
          <div class="col-xs-6">
            
            <div class="table-responsive">
              <table class="table">
                <tr>
                  <th style="width:50%">Subtotal:</th>
                  <td><?= "GHȻ ".(float)$printdata['tot_cost'] ?></td>
                </tr>
                <tr>
                  <th>Tax (<?=$_SESSION['settings']['tax']?>%)</th>
                  <td><?= "GHȻ ".$printdata['tot_cost']  ?></td>
                  <!--<td><?/*= "GHȻ ".$printdata['tot_cost'] * round(($_SESSION['settings']['tax'] / 100),2) */?></td>-->
                </tr>
                <tr>
                  <th>Total:</th>
                  <td><?= "GHȻ ".$printdata['tot_cost'] ?></td>
                </tr>
              </table>
            </div>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </section><!-- /.content -->
    </div><!-- ./wrapper -->

    <!-- AdminLTE App -->
   
  </body>