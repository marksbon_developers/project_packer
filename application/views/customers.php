  <div class="row">
    <div class="col-xs-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div role="tabpanel">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">All Customers</a></li>
                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Add New</a></li>
              </ul>
              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="home">
                  <div class="col-xs-12">
                    <div class="card">
                      <div class="card-body no-padding">
                        <table class="datatable table table-striped primary" cellspacing="0" width="100%" id="allsuppliers">
                          <thead>
                            <tr>
                              <th style="width:6%">ID</th>
                              <th>Customer Name</th>
                              <th>Telephone</th>
                              <th>Location</th>
                              <th>E-mail</th>
                              <th style="width:15%">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                                #Displaying All Suppliers Info
                                if(!empty($all_customers)) {
                                  
                                  $counter = 1;
                                  foreach($all_customers As $customers) {
                                    if($customers->status == "deleted") :  continue; else : ; endif;
                              ?>
                              <tr>
                                <td><?= @$counter; ?></td>
                                <td><?= substr($customers->fullname, 0, 30); ?></td>
                                <td><?= $customers->mobile_number_1; ?></td>
                                <td><?= $customers->location; ?></td>
                                <td><?= $customers->email; ?></td>
                                <td>
                                  <a title="Delete" class="btn btn-primary btn-xs">
                                    <i class="fa fa-pencil"></i> Edit
                                  </a>
                                  <a title="Delete" class="btn btn-danger btn-xs">
                                    <i class="fa fa-pencil"></i> Delete
                                  </a>
                                </td> 
                              </tr> 
                              <?php
                                #
                                    $counter++; }
                                  }
                              ?> 
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
        <div role="tabpanel" class="tab-pane" id="profile">
          <form action="New_Customer" method="post">
          <div class="col-md-4">
          <!-- <label class="col-md-12 control-label" style="font-size:25px !important;">Basic Information</label> -->
          <div class="form-group">
        <label class="col-md-6 control-label">Customers Name</label>
        <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="name" required>
        </div>
      </div>
        <div class="form-group">
        <label class="col-md-6 control-label">Telephone No.</label>
        <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="tel1" required>
        </div>
      </div>
        </div>
         <div class="col-md-4">
         <!-- <label class="col-md-12 control-label" style="font-size:25px !important;">Contact Information</label> -->
          <div class="form-group">
          <label class="col-md-12 control-label">Alt. Telephone No.</label>
          <div class="col-md-12">
            <input type="text" class="form-control" placeholder="" name="tel2">
          </div>
        </div>
        <div class="form-group">
        <label class="col-md-4 control-label">Email</label>
        <div class="col-md-12">
          <input type="email" class="form-control" placeholder="" name="email">
        </div>
      </div>
        </div>
         <div class="col-md-4">
            <!-- <label class="col-md-12 control-label" style="font-size:25px !important;">Purchase Information</label> -->
            
          <div class="form-group">
        <label class="control-label">Postal Address</label>
        <div class="col-md-12">
          <input type="text" class="form-control" name="address" placeholder="P.O.Box ....">
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-12 control-label">Residence Address</label>
        <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="location" required>
        </div>
      </div>
          <!-- <div class="form-group">
        <label class="col-md-6 control-label">Payment Terms</label>
        <div class="col-md-12">
          <select class="select2 form-control" name="pay_type" data-placeholder="--- Select One ---" style="width:100% !important" onchange="paymentterms(this.value)" required>
            <option></option>
            <option>Credit</option>
            <option>Cash</option>
          </select>
        </div>
      </div>
      <div class="form-group " >
        <label class="col-md-6 control-label">Authorized By</label>
          <div class="col-md-12">
          <select class="select2 form-control" name="pay_type" data-placeholder="--- Select One ---" style="width:100% !important" onchange="paymentterms(this.value)" required>
            <option></option>
            <option>Credit</option>
            <option>Cash</option>
          </select>
        </div>
      </div>
      <div class="form-group " >
        <label class="col-md-6 control-label">Balance</label>
        <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="branch">
        </div>
      </div> -->
      

        </div>
         
        <div class="col-md-12" >
          <center>
          <div class="form-footer">
    <div class="form-group">
      <div class="col-md-9 col-md-offset-3">
        <button type="submit" name="customer_save" class="btn btn-primary">Save</button>
        <button type="button" class="btn btn-danger">Cancel</button>
      </div>
    </div>
  </div>
  </center>
        </div>
        </form>
        </div>
        <div role="tabpanel" class="tab-pane" id="messages"> </div>
        <div role="tabpanel" class="tab-pane" id="settings"></div>
    </div>
</div>
    </div>
  </div>

  </div>
</div>

  </div>