<script type="text/javascript">

  function fetch_itms_from_category(descid) {
    $.ajax({
      type: 'POST',
      url: 'prod_ret_ajax_cat',
      data: {desc_id: descid},
      success: function(prod_response){
        if(prod_response)
        {
          document.getElementById("ProductDisplay").innerHTML=prod_response;
        }
        else 
        {
          $('#noproductmodal').modal('show');
        }
      }   
    });
  }

  function addtotally(obj) {
    var prodname = obj.getAttribute('data-prodname');
    var unitprice = obj.getAttribute('data-unitprice');
    var promounitprice = obj.getAttribute('data-promounitprice');
    var promounitqty = obj.getAttribute('data-promounitqty');
    
    $('#tallytbl tr:last').after('<tr><td style="color:red"><i class="fa fa-close del_req_row" style="cursor:pointer"></i></td><td><input type="hidden" name="prodname[]" value="'+prodname+'"/>'+prodname+'</td><td><input type="number" value="0" min="1" class="me qty" name="qty[]" data-unitprice="'+unitprice+'"  data-promo_unit_price="'+promounitprice+'" data-promo_unit_qty="'+promounitqty+'" /></td><td><input type="number" placeholder="1" min="1" class="me unit promocheck" name="unit[]" value="'+unitprice+'" readonly/></td><td><input type="text" placeholder="0" min="1" class="me totalamt" name="totalsum[]" readonly /></td></tr>');

    $(".qty").on('input',compute);
    $(".unit").on('input',compute);
  }

  /******** Unit / Price = Total Calculation **************/
  $(".qty").on('input',compute);

  function compute() {
    var tr = $(this).closest("tr");
    //Promo Check 
    var promo_price = $(this).data('promo_unit_price');
    var promo_qty = $(this).data('promo_unit_qty');

    if(promo_price != 0 && promo_qty != 0 ) {
      var qtyVal = tr.find('.qty').val();
      var unitVal = $(this).data('unitprice');

      if(qtyVal >= promo_qty) {
        tr.find(".unit").val(promo_price);
        var unitVal = tr.find(".unit").val();
      }

      else if(qtyVal < promo_qty) {
        tr.find(".unit").val(unitVal);
        var unitVal = tr.find(".unit").val();
      }
    }
    else {
      var qtyVal = tr.find('.qty').val();
      var unitVal = tr.find(".unit").val();
    }

    if(typeof qtyVal == "undefined" || typeof unitVal == "undefined")
      return;

      tr.find(".totalamt").val(qtyVal * unitVal);
      fnAlltotal();
  }

  function fnAlltotal() {
    var total = 0;
    $(".totalamt").each(function(){
      total += parseFloat($(this).val()||0)
    });

    $("#totalcost").val(total);
    $("#balance").val(total);


    $("#totcost").val(total);
    $(".checkoutamt").text(total);
  }

  function fetch_itms(product_id) {
    $.ajax({
      type: 'POST',
      url: 'prod_ret_ajax',
      data: {productid: product_id},
      success: function(prod_response){
        if(prod_response)
        {
          document.getElementById("ProductDisplay").innerHTML=prod_response;
        }
        else 
        {
          alert("Failed");
        }
      }   
    });
  }

  $(".table").on("click", ".del_req_row", function(){
    $(this).closest('tr').remove();
    fnAlltotal();
  });

  $('.checkout').click(function(){ 
    //$('#checkoutform').submit();
    $('#checkouttable').html($('#tallytable').clone());
    $('#checkoutmodal').modal('show');
  });

  $('#confirmcheckout').click(function(){ 
    $('#checkout_confirmed').submit();
  });

</script>

<!-- Modals -->
<div class="modal fade" id='checkoutmodal' role='dialog' aria-hidden='true' >
  <div class="modal-dialog" style="width: 700px">
    <div class="modal-content">
      <form action="<?= base_url() ?>dashboard/posprint" method="post" name="checkout_confirmed">
        <div class="modal-header" style="padding: 15px">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
          <h4 class="modal-title">Confirm Checkout </h4>
        </div>
        <div class="modal-body" id="checkouttable">
          
        </div>
        <div class="modal-footer">
          <div class="row">
            <div class="col col-md-4">
              <input type="text" name="sold_by" class="form-control" placeholder="Enter Name" required style="background: white">
            </div>
            <div class="col col-md-5">
              <input type="text" name="customer_name" class="form-control" placeholder="Customer Name" required style="background: white">
            </div>
            <div class="col col-md-3">
             <div class="value"> <p class="pull-left" >GHȻ <b class="checkoutamt" style="font-size: 40px"></b> </p></div>
            </div>
          </div>
          
          <div class="col-md-8">
            <button class="btn btn-success" type="submit" id="confirmcheckout"><i class="fa fa-check"></i> Checkout</button>
            <button class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
          </div>
        </div>
        <input type="hidden" name="tot_cost" id="totcost">
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>