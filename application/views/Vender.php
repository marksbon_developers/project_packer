  <div class="row">
    <div class="col-xs-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div role="tabpanel">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">All Suppliers</a></li>
                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Add New</a></li>
              </ul>
              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="home">
                  <div class="col-xs-12">
                    <div class="card">
                      <div class="card-body no-padding">
                        <table class="datatable table table-striped primary" cellspacing="0" width="100%" id="allsuppliers">
                          <thead>
                            <tr>
                              <th>ID</th>
                              <th>Vendor Name</th>
                              <th>Telephone</th>
                              <th>Location</th>
                              <th>E-mail</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                                #Displaying All Suppliers Info
                                if(!empty($allsuppliers)) {
                                  
                                  $counter = 1;
                                  foreach($allsuppliers As $supplier) {
                              ?>
                              <tr>
                                <td><?= @$counter; ?></td>
                                <td><?= substr($supplier->name, 0, 30); ?></td>
                                <td><?= $supplier->tel1; ?></td>
                                <td><?= $supplier->loc; ?></td>
                                <td><?= $supplier->email; ?></td>
                                <td>
                                <form action="" method="Post" style="display:inline !important">
                                <input type="hidden" name="autoid" value="<?= base64_encode($supplier->sup_id); ?>" />
                                  <?php 
                                    if( isset($_SESSION['username']) &&  in_array('SUPCUST',$_SESSION['rows_exploded']) ) {
                                  ?>
                                  <a title="Edit" class='btn btn-primary btn-xs supdetails' data-supid="<?= base64_encode($supplier->sup_id) ?>" data-name="<?=  $supplier->name; ?>" data-addr="<?= $supplier->addr ?>" data-tel1="<?= $supplier->tel1 ?>" data-tel2="<?= $supplier->tel2 ?>"  data-email="<?= $supplier->email ?>" data-loc="<?= $supplier->loc ?>" data-paytype="<?= $supplier->pay_type ?>" data-prodtype="<?= $supplier->prod_type ?>" data-bank="<?= $supplier->bank ?>" data-branch="<?= $supplier->branch ?>" data-acctname="<?= $supplier->acctname ?>" data-num="<?= $supplier->num ?>">
                                    <i class="fa fa-pencil"></i> Edit 
                                  </a>
                                  <?php 
                                        }
                                    if( isset($_SESSION['username']) &&  in_array('SUPCUST',$_SESSION['rows_exploded']) ) {
                                  ?>
                                  <a title="Delete" class="btn btn-danger btn-xs deletebtn" data-toggle="modal" data-delname="<?= $supplier->name; ?>" data-delid="<?= base64_encode($supplier->sup_id); ?>" data-formurl="Delete_Vendor">
                                    <i class="fa fa-trash"></i> Delete
                                  </a>
                                  <?php } ?>
                                </form>
                                <button type='button' name='user_details' class='btn btn-info btn-xs supdetails' data-supid="<?= base64_encode($supplier->sup_id) ?>" data-name="<?=  $supplier->name; ?>" data-addr="<?= $supplier->addr ?>" data-tel1="<?= $supplier->tel1 ?>" data-tel2="<?= $supplier->tel2 ?>"  data-email="<?= $supplier->email ?>" data-loc="<?= $supplier->loc ?>" data-paytype="<?= $supplier->pay_type ?>" data-prodtype="<?= $supplier->prod_type ?>" data-bank="<?= $supplier->bank ?>" data-branch="<?= $supplier->branch ?>" data-acctname="<?= $supplier->acctname ?>" data-num="<?= $supplier->num ?>"><i class='fa fa-th'></i> Details</button>
                                </td> 
                              </tr> 
                              <?php
                                #
                                    $counter++; }
                                  }
                              ?> 
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
        <div role="tabpanel" class="tab-pane" id="profile">
          <form action="New_Vendor" method="post">
          <div class="col-md-4">
          <label class="col-md-12 control-label" style="font-size:25px !important;">Basic Information</label>
          <div class="form-group">
        <label class="col-md-6 control-label">Vendor's Name</label>
        <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="name" required>
        </div>
      </div>
        <div class="form-group">
        <label class="col-md-6 control-label">Telephone No.</label>
        <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="tel1" required>
        </div>
      </div>
        <div class="form-group">
        <label class="col-md-12 control-label">Alt. Telephone No.</label>
        <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="tel2">
        </div>
      </div>
        </div>
         <div class="col-md-4">
         <label class="col-md-12 control-label" style="font-size:25px !important;">Contact Information</label>
          <div class="form-group">
        <label class="col-md-4 control-label">Address</label>
        <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="addr">
        </div>
      </div>
        <div class="form-group">
        <label class="col-md-4 control-label">Email</label>
        <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="email">
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-12 control-label">Location</label>
        <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="loc" required>
        </div>
      </div>
        </div>
         <div class="col-md-4">
             <label class="col-md-12 control-label" style="font-size:25px !important;">Purchase Information</label>
         
          <div class="form-group">
        <label class="col-md-6 control-label">Payment Terms</label>
        <div class="col-md-12">
          <select class="select2 form-control" name="pay_type" data-placeholder="--- Select One ---" style="width:100% !important" onchange="paymentterms(this.value)" required>
            <option></option>
            <option>Cheque</option>
            <option>Cash</option>
          </select>
        </div>
      </div>
      <div class="form-group bank" style="display:none">
        <label class="col-md-6 control-label">Bank Name</label>
        <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="bank">
        </div>
      </div>
      <div class="form-group branch" style="display:none">
        <label class="col-md-6 control-label">Branch</label>
        <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="branch">
        </div>
      </div>
      <div class="form-group accname" style="display:none">
        <label class="col-md-6 control-label">Account Name</label>
        <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="acc_name">
        </div>
      </div>
      <div class="form-group accno" style="display:none">
        <label class="col-md-4 control-label">Account No.</label>
        <div class="col-md-12">
          <input type="text" class="form-control" placeholder="" name="acc_no">
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-4 control-label">Product Type</label>
        <div class="col-md-12">
          <select class="select2" multiple data-placeholder="--- Select One ---" style="width:100% !important" name="productname[]" >
            <option></option>
            <?php
              if(!empty($category_info)) 
              {
                $counter = 1;
                foreach($category_info As $cat) 
                {
                  print "<option value='$cat->cat_id'>$cat->cat_name</option>";
                }
              }
            ?>
          </select>
        </div>
      </div>

        </div>
         
        <div class="col-md-12" >
          <center>
          <div class="form-footer">
    <div class="form-group">
      <div class="col-md-9 col-md-offset-3">
        <button type="submit" name="sup_save" class="btn btn-primary">Save</button>
        <button type="button" class="btn btn-danger">Cancel</button>
      </div>
    </div>
  </div>
  </center>
        </div>
        </form>
        </div>
        <div role="tabpanel" class="tab-pane" id="messages"> </div>
        <div role="tabpanel" class="tab-pane" id="settings"></div>
    </div>
</div>
    </div>
  </div>

  </div>
</div>

  </div>