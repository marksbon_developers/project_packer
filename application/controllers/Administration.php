
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administration extends CI_Controller 
{
	/************************************ Constructor / Logout ******************************/
		/*******************************
  		    Administration Index 
  	*******************************/
  	public function index() 
    {
  		# Permission Check
      if(isset($_SESSION['username']))
        redirect('Dashboard');
      
      else
        redirect('access/login');
      
  	}
  /************************************ Constructor / Logout ******************************/

	/***************************************	Interfaces 	***********************************/
		/******************************
			Manage Users
		*******************************/
		public function Users() 
		{
      if(isset($_SESSION['username']))
			{
				if(in_array('USERS',$_SESSION['rows_exploded'])) 
				{
					# Loading models
          $this->load->model('Model_Access');/*Needed By header, nav*/
          $this->load->model('Universal_Retrieval');
          $this->load->model('Model_HR');

          $this->load->library('encryption');
          
          #Extracting Data For Display
          $data['allemployees']  = $this->Universal_Retrieval->All_Info('hr_emp_pers_info');
          $data['allusers']      = $this->Universal_Retrieval->All_Info('access_user_details');
          $data['allgroups']     = $this->Universal_Retrieval->All_Info('access_roles_priviledges_group');
          $data['dash_tabs']     = $this->Model_Access->dashboard_tabs();

					/********** Generating New User Id ************/
          $last_employee_id  = $this->Model_HR->ret_last_employee_id();

          $data['id'] = $last_employee_id->id;
          
          if( !empty($last_employee_id) )
          {
            if($last_employee_id->employee_id == NULL || $last_employee_id->employee_id == 0) 
              $data['next_usr_id'] = "KAD/TEMP/001";
            
            elseif( strlen($last_employee_id->employee_id) == 1 )
              $data['next_usr_id'] = "KAD/TEMP/00".($last_employee_id->employee_id + 1);

            elseif( strlen($last_employee_id->employee_id) == 2 )
              $data['next_usr_id'] = "KAD/TEMP/0".($last_employee_id->employee_id + 1);

            elseif( strlen($last_employee_id->employee_id) == 3 )
              $data['next_usr_id'] = "KAD/TEMP/".($last_employee_id->employee_id + 1);
          }
          else
          {
            $this->session->set_flashdata('error',"Error In Retrieving Last Employee ID");
            $data['next_usr_id'] = "ERROR";
          }  
          /********** Generating New User Id ************/

					/********** Interface ***********************/    
          $headertag['title'] = "Users";
          $this->load->view('headtag',$headertag);
          $this->load->view('access/users',$data);
          $this->load->view('footer');
          /********** Interface ***********************/
				}
		 		else 
		 		{
					$this->session->set_flashdata('error', "Permission Denied. Contact Administrator");
					redirect('dashboard');
				}
			}
			else
			{
				redirect('access');
			}
		}

    /******************************
      User Priviledges
    *******************************/
    public function Priviledges() 
    {
      if(isset($_SESSION['username']))
      {
        if(in_array('ROLES',$_SESSION['rows_exploded'])) 
        {
          # Loading models
          $this->load->model('Model_Access');/*Needed By header, nav*/
          $this->load->model('Universal_Retrieval');
          $this->load->model('Model_HR');

          #Extracting Data For Display
          $data['allusers']  = $this->Universal_Retrieval->All_Info('access_user_details');

          /********** Interface ***********************/    
          $headertag['title'] = "User Priviledges";
          $this->load->view('headtag',$headertag);
          $this->load->view('Priviledges',$data);
          $this->load->view('Priviledges_pagesettings',$data);
          $this->load->view('footer');
          /********** Interface ***********************/
        }
        else 
        {
          $this->session->set_flashdata('error', "Permission Denied. Contact Administrator");
          redirect('dashboard');
        }
      }
      else
      {
        redirect('access');
      }
    }
    
    /******************************
  		Settigs
  	*******************************/
  	public function Settings() 
    {
  		if(isset($_SESSION['username']))
      {
        if(in_array('SETTINGS', $_SESSION['rows_exploded'])) 
        {
          # Loading models...
            $this->load->model('Model_Access'); /*Needed By header, nav*/
            $this->load->model('Universal_Retrieval');

          # Extracting Data For Display
            $data['companyinfo'] = $this->Universal_Retrieval->All_Info_Row("hr_companyinfo");

  		    /********** Interface ***********************/    
            $headertag['title'] = "System Settings";
            $this->load->view('headtag',$headertag);
            $this->load->view('settings',$data);
            $this->load->view('footer');
          /********** Interface ***********************/
  		  } 
        else 
        {
  		    $this->session->set_flashdata('error',"Permission Denied. Contact Administrator");
  		    redirect('Dashboard');
  		  }
      }
      else
        redirect('access/login');
  	}
    
    /***********************************************
  		Report
  	************************************************/
  	public function Report() 
    {
  		if (isset($_SESSION['username']))
      {
        if(in_array('REPORT',$_SESSION['rows_exploded']))
        {
          # Loading models...
          $this->load->model('Model_Access'); /*Needed By header, nav*/
          $this->load->model('Universal_Retrieval');
              
          /********** Interface ***********************/    
          $headertag['title'] = "Audit";
          $this->load->view('headtag',$headertag);
          $this->load->view('header');
          $this->load->view('nav');
          $this->load->view('administration/Report');
          $this->load->view('footer');
          /********** Interface ***********************/
  		  } 
        else 
        {
          $this->session->set_flashdata('error',"Permission Denied. Contact Administrator");
          redirect('dashboard');
        }
      }
      else
      {
        redirect('access/login');
      }
  	}  

  /***************************************  Interfaces  ***********************************/
  
	/***************************	Data Insertion	*****************************/
    /*****************************
	       New User
    *****************************/
    public function Add_User() 
    {
      if(isset($_SESSION['username']) )
      {
        if(in_array('USERS',$_SESSION['rows_exploded']))
        {
          if(isset($_POST['add_usr'])) 
          {
            $this->form_validation->set_rules('id','Generated Table ID','required|trim');
            $this->form_validation->set_rules('employee_id','Employee ID','required|trim|is_unique[hr_emp_pers_info.employee_id]');
            $this->form_validation->set_rules('fullname','Full Name','required|trim');
            $this->form_validation->set_rules('dob','Date Of Birth','trim');
            $this->form_validation->set_rules('gender','Gender','trim');
            $this->form_validation->set_rules('emergencyname',"Referee's Name",'trim');
            $this->form_validation->set_rules('emergencytel',"Referee's Tel",'trim');
            $this->form_validation->set_rules('residenceAddr',"Residence Address",'trim');
            $this->form_validation->set_rules('tel1',"Telephone Number",'required|trim');
            $this->form_validation->set_rules('tel2',"Alt. Telephone Number",'trim');
            $this->form_validation->set_rules('email',"Email",'trim');
            $this->form_validation->set_rules('usr_name','User Name','required|trim|is_unique[access_users.username]',array('is_unique[users.username]' => 'Error Message on rule2 for this field_name'));
            $this->form_validation->set_rules('usr_pwd_1','Password','trim|required|min_length[5]|max_length[12]');
            $this->form_validation->set_rules('usr_pwd_2','Confirm Password','trim|required|min_length[5]|max_length[12]|matches[usr_pwd_1]');

            # If Failed
            if($this->form_validation->run() === FALSE) 
            {
              $this->session->set_flashdata('error',validation_errors());
              redirect('Administration/Users');
            }
            # If Passed
            else 
            {
              $passwd_1   = $this->input->post('usr_pwd_1');
              $passwd_2   = $this->input->post('usr_pwd_2');
              $id         = $this->input->post('id');
              
              if(strcmp($passwd_1, $passwd_2) == 0) 
              {
                $this->load->model('Universal_Insertion');
                $this->load->model('Model_Access');
                $this->load->model('Model_HR');

                $user_info = [
                  'employee_id'    => $this->input->post('employee_id'),
                  'fullname'  => ucwords($this->input->post('fullname')),
                  'dob'       => $this->input->post('dob'),
                  'gender'    => $this->input->post('gender'),
                  'residence' => $this->input->post('residenceAddr'),
                  'tel1'      => $this->input->post('tel1'),
                  'tel2'      => $this->input->post('tel2'),
                  'email'     => $this->input->post('email'),
                  'eme_name'  => $this->input->post('emergencyname'),
                  'eme_tel'   => $this->input->post('emergencytel')
                ];

                # Transaction Begin
                $this->db->trans_start();
                  $this->Universal_Insertion->DataInsert('hr_emp_pers_info', $user_info);
                
                  $last_emp_id = $this->db->insert_id();

                  $login_Info = [
                    'id'        => $last_emp_id,
                    'employee_id'    => $this->input->post('employee_id'),
                    'username'  => $this->input->post('usr_name'),
                    'passwd'    => $this->Model_Access->encrypt_password($passwd_1),
                    'login_attempt' => '5'
                  ];
                
                  $this->Universal_Insertion->DataInsert('access_users', $login_Info);

                  $user_roles = [ 'employee_id' => $last_emp_id ];

                  $this->Universal_Insertion->DataInsert('access_roles_priviledges_user', $user_roles);

                  $this->Model_HR->last_employee_id_increase($last_emp_id);
                $this->db->trans_complete();
                # Transaction End
              
                if($this->db->trans_status() === FALSE)
                  $this->session->set_flashdata('error','User Registration Failed');
                
                else
                  $this->session->set_flashdata('success','User Successfully Added');
                
                redirect('administration/users');
              }
              else
              {
                $this->session->set_flashdata('error','Password Do Not Match.');
                redirect('administration/users');
              }
            }
          }
        }
        else
        {
          $this->session->set_flashdata('error','Permission Denied. Contact Administrator');
          redirect('dashboard');
        }
      }
      else
      {
        redirect('access');
      }
    }

    /***********************************************
         Active / De-Activate User
    ************************************************/
    public function Activation() 
    {
      if(isset($_SESSION['username']) && in_array('USERS',$_SESSION['rows_exploded']) && isset($_POST['activate_usr'])) 
      {
        $this->form_validation->set_rules('username','User','required|trim');
            
        if ($this->form_validation->run() === FALSE) 
        {
          $this->session->set_flashdata('error',"No User Selected.");
          redirect('administration/users');
        }
        
        else 
        {
          $this->load->model('Model_Access');

          $result = $this->Model_Access->deactivate_account($this->input->post('username'),1);
                
          if($result) 
            $this->session->set_flashdata('success',"User Activated.");  
         
         else 
            $this->session->set_flashdata('success',"User Activated.");  
              
          redirect('administration/users');
        }
      } 
        
      elseif(isset($_SESSION['username']) && in_array('USERS',$_SESSION['rows_exploded']) && isset($_POST['deactivate_usr'])) 
      {
        //Setting validation rules
        $this->form_validation->set_rules('username','User','required|trim');
        
        if ($this->form_validation->run() === FALSE) 
        {
          $this->session->set_flashdata('error',"No User Selected.");
          redirect('administration/users');
        }
        
        else 
        {
          $this->load->model('Model_Access');

          $result = $this->Model_Access->deactivate_account($this->input->post('username'),0);
                
          if($result) 
            $this->session->set_flashdata('success',"User Deactived.");  
         
         else 
            $this->session->set_flashdata('error',"Deactivation Failed.");  
              
          redirect('administration/users');

        }
      }
 
      else
      {
        $this->session->set_flashdata('error',"Permission Denied. Contact Administrator");
        redirect('administration/users');
      }
    }

    /***********************************************
         Active / De-Activate User
    ************************************************/
    public function ResetPassword() 
    {
      if(isset($_SESSION['username']) && in_array('USERS',$_SESSION['rows_exploded']) && isset($_POST['resetbtn'])) 
      {
        //Setting validation rules
        $this->form_validation->set_rules('userResetId','User','required|trim');
        $this->form_validation->set_rules('newpasswd','Password','required|trim');
        $this->form_validation->set_rules('confirmpasswd','Confirm Password','required|trim');
        
        if ($this->form_validation->run() === FALSE) 
        {
          $this->session->set_flashdata('error',"No User Selected.");
          redirect('administration/users');
        }
        
        else 
        {
          $this->load->model('Model_Access');

          if($this->input->post('newpasswd') == $this->input->post('confirmpasswd'))
          {
            $username = base64_decode($this->input->post('userResetId'));
            $password = $this->input->post('newpasswd');

            $result = $this->Model_Access->Reset_Password($username,$password);
                  
            if($result) 
              $this->session->set_flashdata('success',"Password Reset Successful");  
           
            else 
              $this->session->set_flashdata('error',"Password Reset Failed."); 
          }
          else
            $this->session->set_flashdata('error',"Password Mismatch. Re-enter");  

          redirect('administration/users');
        }
      }
      else
      {
        $this->session->set_flashdata('error',"Permission Denied. Contact Administrator");
        redirect('administration/users');
      }
    }
    
    /***********************************************
	       Roles Assignment  
	  ************************************************/
    public function Assign_Roles() 
    {
      if(isset($_SESSION['username']))
      {
        if(isset($_POST['assign']) && in_array('ROLES',$_SESSION['rows_exploded'])) 
        {
  		    # Setting validation rules
          $this->form_validation->set_rules('username','User Role(s)','required|trim');
	        $this->form_validation->set_rules('roles[]','User Role(s)','required|trim');
            
          # Checking Form Validation Rule Result
          if($this->form_validation->run() === FALSE) 
          {
            $this->session->set_flashdata('error',validation_errors());
            redirect('administration/priviledges');
          }
          # If Passed
          else 
          {
            # Loading model...
            $this->load->model('Universal_Update');

            $userid = base64_decode($this->input->post('username'));
          	$Roles = $this->input->post('roles[]');
            
	         if(!empty($Roles))
            {
              foreach($Roles As $role_array) 
              {
                @$data['roles'] .= $role_array."|";
              } 

              $where_condition = ['employee_id' => $userid];
              
              # Roles Insertion
              $result = $this->Universal_Update->Multiple_Update('access_roles_priviledges_user',$data,$where_condition);

              if($result)
                $this->session->set_flashdata('success','Roles Assigment Successful');

              else
                $this->session->set_flashdata('error','Roles Assignment Failed');

              redirect('administration/priviledges');
            }
          }
		    } 
    		else
        {
    		 	$this->session->set_flashdata('error','Permission Denied. Contact Administrator');
    			redirect('administration/priviledges');
    		}
      }
      else
        redirect('access/login');
    }

    /*****************************
      Company Registration
    *****************************/
    public function Register_Company() 
    {
      if(isset($_SESSION['username']) )
      {
        if(in_array('SETTINGS',$_SESSION['rows_exploded']) && isset($_POST['reg_company']))
        {
            $this->form_validation->set_rules('companyname','Company Name','required|trim');
            $this->form_validation->set_rules('paddr','Postal Address','required|trim');
            $this->form_validation->set_rules('raddr','Residential Address','required|trim');
            $this->form_validation->set_rules('tel1','Primary Telephone','required|trim');
            $this->form_validation->set_rules('tel2','Secondary Telephone','trim');
            $this->form_validation->set_rules('email',"Email",'trim');
            $this->form_validation->set_rules('website',"Website",'trim');
            $this->form_validation->set_rules('fax',"Fax",'trim');

            # If Failed
            if($this->form_validation->run() === FALSE) 
            {
              $this->session->set_flashdata('error',validation_errors());
              redirect('administration/settings');
            }
            # If Passed
            else 
            {
              # Loading Models
              $this->load->model('Universal_Insertion');
              $this->load->helper('file_restriction');

              # Working on File Upload $file_array,$mall_name,$img_type
              if( !empty($_FILES['logo']['tmp_name']) && !empty($this->input->post('companyname')) ) 
              {
                $target_dir = "resources/images/";
                $error_url  = base_url()."administration/Settings";

                if( $logo_path = pic_restriction($_FILES['logo'],$this->input->post('companyname'),$target_dir,$error_url) )
                {
                  if(!empty($logo_path))
                    move_uploaded_file($_FILES['logo']['tmp_name'], $logo_path);
                }

                else
                  redirect('administration/settings');
              }
              else
                $logo_path = base_url()."resources/default_logo.png";
              # End of Logo Upload

              $company_info = [
                'name'     => ucwords($this->input->post('companyname')),
                'tel_1'    => $this->input->post('tel1'),
                'tel_2'    => $this->input->post('tel2'),
                'fax'      => $this->input->post('fax'),
                'email'    => $this->input->post('email'),
                'website'  => $this->input->post('website'),
                'address'  => ucwords($this->input->post('paddr')),
                'location' => ucwords($this->input->post('raddr')),
                'logo_path'=> $logo_path
              ];

              $result = $this->Universal_Insertion->DataInsert('hr_companyinfo', $company_info);
                
              if($result)
                $this->session->set_flashdata('success','Company Registration Successfully');
                
              else
                $this->session->set_flashdata('error','Company Registration Failed');
                
              redirect('administration/settings');
            }
        }
        else
        {
          $this->session->set_flashdata('error','Permission Denied. Contact Administrator');
          redirect('dashboard');
        }
      }
      else
        redirect('access/login');
    }

  /***************************  Data Insertion  *****************************/
    


  /*********************************  Data Update **************************/
    /*****************************
         Employee Info
    *****************************/
    public function Employee_Info_Update() 
    {
      if(isset($_SESSION['username']) )
      {
        if(in_array('USERS',$_SESSION['rows_exploded']))
        {
          if(isset($_POST['update_usr'])) 
          {
            $this->form_validation->set_rules('last_id','Generated Table ID','required|trim');
            $this->form_validation->set_rules('edit_employee_id','Employee ID','required|trim');
            $this->form_validation->set_rules('edit_fullname','Full Name','required|trim');
            $this->form_validation->set_rules('edit_dob','Date Of Birth','trim');
            $this->form_validation->set_rules('edit_gender','Gender','trim');
            $this->form_validation->set_rules('edit_emergencyname',"Referee's Name",'required|trim');
            $this->form_validation->set_rules('edit_emergencytel',"Referee's Tel",'required|trim');
            $this->form_validation->set_rules('edit_resAddr',"Residence Address",'required|trim');
            $this->form_validation->set_rules('edit_tel1',"Telephone Number",'required|trim');
            $this->form_validation->set_rules('edit_tel2',"Alt. Telephone Number",'required|trim');
            $this->form_validation->set_rules('edit_email',"Email",'trim');

            # If Failed
            if($this->form_validation->run() === FALSE) 
            {
              $this->session->set_flashdata('error',validation_errors());
              redirect('administration/users');
            }
            # If Passed
            else 
            {
              # Loading Models 
              $this->load->model('Universal_Insertion');
              $this->load->model('Model_Access');
              $this->load->model('Model_HR');

              $user_info = [
                'employee_id' => $this->input->post('edit_employee_id'),
                'fullname'    => ucwords($this->input->post('edit_fullname')),
                'dob'         => $this->input->post('edit_dob'),
                'gender'      => $this->input->post('edit_gender'),
                'residence'   => $this->input->post('edit_resAddr'),
                'tel1'        => $this->input->post('edit_tel1'),
                'tel2'        => $this->input->post('edit_tel2'),
                'email'       => $this->input->post('edit_email'),
                'eme_name'    => $this->input->post('edit_emergencyname'),
                'eme_tel'     => $this->input->post('edit_emergencytel')
              ];

              # Updating User Info
              $result = $this->Model_HR->Update_Employee_info($user_info,$this->input->post('last_id'));
              
              
              if($result)
                $this->session->set_flashdata('success','Employee Update Successful');
                
              else
                $this->session->set_flashdata('error','Employee Update Failed');
                
              redirect('administration/users');
            }
          }
        }
        else
        {
          $this->session->set_flashdata('error','Permission Denied. Contact Administrator');
          redirect('dashboard');
        }
      }
      else
      {
        redirect('access');
      }
    }

    /*****************************
      Company Registration
    *****************************/
    public function Update_Company() 
    {
      if(isset($_SESSION['username']) )
      {
        if(in_array('SETTINGS',$_SESSION['rows_exploded']) && isset($_POST['up_company']))
        {
            $this->form_validation->set_rules('id','Company ID','required|trim');
            $this->form_validation->set_rules('companyname','Company Name','required|trim');
            $this->form_validation->set_rules('paddr','Postal Address','required|trim');
            $this->form_validation->set_rules('raddr','Residential Address','required|trim');
            $this->form_validation->set_rules('tel1','Primary Telephone','required|trim');
            $this->form_validation->set_rules('tel2','Secondary Telephone','trim');
            $this->form_validation->set_rules('email',"Email",'trim');
            $this->form_validation->set_rules('website',"Website",'trim');
            $this->form_validation->set_rules('fax',"Fax",'trim');

            # If Failed
            if($this->form_validation->run() === FALSE) 
            {
              $this->session->set_flashdata('error',validation_errors());
              redirect('administration/settings');
            }
            # If Passed
            else 
            {
              # Loading Models
              $this->load->model('Universal_Update');
              $this->load->helper('file_restriction');

              # Working on File Upload $file_array,$mall_name,$img_type
              if( !empty($_FILES['logo']['tmp_name']) && !empty($this->input->post('companyname')) ) 
              {
                $target_dir = "resources/images/";
                $error_url  = base_url()."administration/Settings";

                if( $logo_path = pic_restriction($_FILES['logo'],$this->input->post('companyname'),$target_dir,$error_url) )
                {
                  if(!empty($logo_path))
                    move_uploaded_file($_FILES['logo']['tmp_name'], $logo_path);
                }

                else
                  redirect('Administration/Settings');
              }
              else
                $logo_path = base_url()."resources/default_logo.png";
              # End of Logo Upload

              $company_info = [
                'name'     => ucwords($this->input->post('companyname')),
                'tel_1'    => $this->input->post('tel1'),
                'tel_2'    => $this->input->post('tel2'),
                'fax'      => $this->input->post('fax'),
                'email'    => $this->input->post('email'),
                'website'  => $this->input->post('website'),
                'address'  => ucwords($this->input->post('paddr')),
                'location' => ucwords($this->input->post('raddr')),
                'logo_path'=> $logo_path
              ];

              $where_condition = ['id' => base64_decode($this->input->post('id'))];

              $result = $this->Universal_Update->Multiple_Update('hr_companyinfo', $company_info,$where_condition);
                
              if($result)
                $this->session->set_flashdata('success','Company Info Updated');
                
              else
                $this->session->set_flashdata('error','Company Update Failed');
                
              redirect('administration/settings');
            }
        }
        else
        {
          $this->session->set_flashdata('error','Permission Denied. Contact Administrator');
          redirect('dashboard');
        }
      }
      else
        redirect('access/login');
    }
  
  /*********************************  Data Update **************************/



  /*********************************	Data Delete	**************************/
    /***********************************
	       Delete User
	  ************************************/
    public function Delete_User() 
    {
      if(isset($_SESSION['username']))
      {
        if(in_array('USERS',$_SESSION['rows_exploded']) && isset($_POST['deletebutton']))
        {
          $this->form_validation->set_rules('deleteid','User','required|trim');

          if($this->form_validation->run() === FALSE) 
          {
            $this->session->set_flashdata('error','No User Selected');
            redirect('Administration');
          }
          else
          {
            $this->load->model('Model_Access');

            $userid = $this->input->post('deleteid');

            $result = $this->Model_Access->delete_user($userid);
                
            if($result) 
              $this->session->set_flashdata('success',"User Deleted");
            
            else 
              $this->session->set_flashdata('error','Delete Failed');
            
            redirect('administration/users');
          }
        }
        else
        {
          $this->session->set_flashdata('error','Permission Denied. Contact Administrator');
          redirect('administration/users');
        }
      }
      else
        redirect('access');
    }

    /***********************************
         Complete Employee Delete
    ************************************/
    public function Del_Employee_Complete() 
    {
      if(isset($_SESSION['username']))
      {
        if(in_array('USERS',$_SESSION['rows_exploded']) && isset($_POST['deletebutton']))
        {
          $this->form_validation->set_rules('deleteid','User','required|trim');

          if($this->form_validation->run() === FALSE) 
          {
            $this->session->set_flashdata('error','No User Selected');
            redirect('administration/users');
          }
          else
          {
            $this->load->model('Model_Access');

            $employee_id = $this->input->post('deleteid');

            $result = $this->Model_Access->delete_employee($employee_id);
                
            if($result) 
              $this->session->set_flashdata('success',"Employee Deleted");
            
            else 
              $this->session->set_flashdata('error','Delete Failed');
            
            redirect('administration/users');
          }
        }
        else
        {
          $this->session->set_flashdata('error','Permission Denied. Contact Administrator');
          redirect('administration/users');
        }
      }
      else
        redirect('Access');
    }
  
  /*********************************  Data Delete **************************/
  

    
}//End of Class
