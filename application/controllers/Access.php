<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Access extends CI_Controller 
{
	/************************************** Interfaces ********************************/
  	/*******************************
             Constructor
    *******************************/
    public function index() 
  	{
  		redirect('access/login');
  	}

    /*******************************
        Login Page - Normal User
    *******************************/
    public function login() 
    {
      if( isset($_SESSION['username']) )
      {
        if(isset($_SESSION['rows_exploded']))
          redirect('dashboard');
        else
        {
          session_destroy();
          redirect('access/login');
        }
      }
      else 
      {
        # Load Login View
        $title['title'] = "Login";
        $this->load->view('access/login',$title);
      }
    }

    /*******************************
           Lock Screen
    *******************************/
    public function lockscreen() 
    {
      if ( isset($_SESSION['rows_exploded']) ) 
      {
        # Freezing Session
        $_SESSION['login_state'] = "Freezed";
        
        $headertag['title'] = "LockScreen";
        $this->load->view('lockscreen',$headertag);
      } 
      else 
      {
        # Not Logged In
        redirect('access');
      }
    }

  /************************************** Interfaces ********************************/


  /************************************** Other Func ********************************/
    /*******************************
           Logout
    *******************************/
    public function logout() 
    {   
      if( isset($_SESSION['username']) && isset($_SESSION['LoginID']) ) 
      {
        $this->load->model('Model_Access');

        $result = $this->Model_Access->logout_log($_SESSION['LoginID']);
        
        if( $result ) 
        {
          session_destroy();
          redirect('access');
        } 
        
        else 
        {
          $this->session->set_flashdata('error',"Log Out Failed");
          redirect('dashboard');
        }
      }
    }

  	/***********************************************
  		Password Encyption and Verifying functions
  	************************************************/
  	public function encrypt_password($password) 
    {
  		# Password encryption
  		$encrypted_passwd = password_hash($password,PASSWORD_BCRYPT);
  			
  		return ($encrypted_passwd);
  	}
	
  	public function verify_password($password,$encrypted_password) 
    {
  		# Replacing double quotes of hash to single
  		$encrypted_password = str_replace('"',"''",$encrypted_password);
  		
  		# password verfication and returning to function
  		return((password_verify($password,$encrypted_password)) ? true : false);
  	}
    
    /***********************************************
		  Password Verification From LockScreen
    ************************************************/
    public function password_verify() 
    {
		  if(isset($_POST['passwd_verify'])) 
      {
    		# Loading Model
        $this->load->model('UserModel');
        
        # Validation rules
        $this->form_validation->set_rules('username', 'Username', 'required|trim');
        $this->form_validation->set_rules('passwd', 'Password', 'required|trim');
        
        # Assignment
        $username = $this->input->post('username');
        $password = $this->input->post('passwd');
        
        # Password encryption
    		$encrypted_passwd = password_hash($password,PASSWORD_BCRYPT);
            
        $data = $this->UserModel->dblookup($username);
        
        if($data) 
        {
          # Performing Password Verfication 
          if($this->verify_password($password, $data['encrypted_password'])) 
          {
            # Unlock System
            $_SESSION['login_state'] = "Continue";
            redirect('dashboard');
          }
          else 
          {
            $_SESSION['error'] = "Incorrect Password.";
            redirect('access/lockscreen');
          }
        }
      }
    }

  	/***********************************************
  		          Login Validation 
  	************************************************/
  	public function login_validation() 
    {
	    # Login Button Clicked
      if(isset($_POST['login'])) 
      {
        # Validation Rules
        $this->form_validation->set_rules('username', 'Username', 'required|trim');
        $this->form_validation->set_rules('passwd', 'Password', 'required|trim');

        # Validation Test Fail
        if($this->form_validation->run() === FALSE) 
        {
          $this->session->set_flashdata('error','All Fields Required');
          redirect('access/login');
        }
 	      else 
        {
				  # Loading Models
	        $this->load->model('Model_Access');
          $this->load->model('Model_HR');
          $this->load->model('Universal_Retrieval');
          $this->load->model('Universal_Update'); 

          # Performing Database Verfication ==> Username
          $username = $this->input->post('username',TRUE);
          $password = $this->input->post('passwd',TRUE);
                
				  $data = $this->Model_Access->verify_username('access_users',$username);

          if(!empty($data))
          {
            # Perform Password Check
            if($this->verify_password($password, $data['encrypted_password'])) 
            { 
              # Verify Account Status and Login Attempts
              if($data['account_active'] == 1 && $data['login_attempt'] > 0) 
              {
                /************************ Retrieving Company Info ********************/
                  $companyinfo = $this->Universal_Retrieval->All_Info_Row('hr_companyinfo'); 
                    
                  if(!empty($companyinfo)) 
                  {
                    # Storing in Memory
                    $session_array = 
                    [  
                      'company_name'     => $companyinfo->name,
                      'company_tel'      => $companyinfo->tel_1,
                      'company_alttel'  => $companyinfo->tel_2,
                      'company_fax'      => $companyinfo->fax,
                      'company_email'    => $companyinfo->email,
                      'company_address'  => $companyinfo->address,
                      'company_website'  => $companyinfo->website,
                      'company_location'  => $companyinfo->location,
                      'company_logo'  => $companyinfo->logo_path
                    ];
                  }
                  # No Company Info Retrieved So Assign To Show New Company.
                  else
                    $session_array = [ 'companyname' => "Unregisted"] ;
                /************************ End of Company Info ********************/

                /************************ User Roles & Priviledges ********************/
                  $roles_priv = $this->Model_Access->roles_priviledges_user('access_roles_priviledges_user',$data['id']);
            
                  if(!empty($roles_priv)) 
                  {
                    $roles_unexploded = $roles_priv->roles;
                    
                    //$priv_unexploded  = $roles_priv->priviledges;

                    //$group_id         = $roles_priv->group_id;

                    if(empty($roles_unexploded)) //empty($group_id) && empty($priv_unexploded) && 
                    {
                      $this->session->set_flashdata('error','No Permissions Set For User. Contact Administrator');
                      redirect('access/login');
                    }

                    /*if(!empty($group_id))
                    {
                      $roles_priv_retrieve = $this->Model_Access->roles_priviledges_group('access_roles_priviledges_group',$group_id);

                      if(!empty($roles_priv_retrieve))
                      {
                        $temp_array['rows_unexploded'] = $roles_priv_retrieve->roles;
                        $temp_array['priv_unexploded'] = $roles_priv_retrieve->priviledges;

                        $session_array['rows_exploded'] = explode("|", $temp_array['rows_unexploded']);
                        $session_array['priv_exploded'] = explode("|", $temp_array['priv_unexploded']);
                      }
                    }*/

                    if(!empty($roles_unexploded)) // || !empty($priv_unexploded))
                    {
                      @$temp_array['rows_unexploded'] .= $roles_unexploded;
                      //$temp_array['priv_unexploded'] .= $priv_unexploded;

                      $session_array['rows_exploded'] = explode("|", $temp_array['rows_unexploded']);
                      //$session_array['priv_exploded'] = explode("|", $temp_array['priv_unexploded']);
                    }

                  }
                  
                  else
                  {
                    $this->session->set_flashdata('error','No Permissions Set For User. Contact Administrator');
                    redirect('access/login');
                  }
                /************************ End User Roles & Priviledges ****************/

                /************************ Employee's Personal Info  ********************/
                  $employee_data = $this->Model_HR->employee_personal_data($data['id']);
                  
                  if (!empty($employee_data)) 
                  {
                    #Storing in Session
                    $session_array [ 'employee_personal_data' ]  = $employee_data;
                    $session_array [ 'fullname' ]                = $employee_data->fullname;
                    $session_array [ 'employee_id' ]             = $employee_data->employee_id;
                    $session_array [ 'username']                 = $username;
                  }
                  else
                    $this->session->set_flashdata('error',"Employee Personal Data Loading Failed<br>");
                /************************ Employee's Personal Info  ********************/

                /************************ Recording Login Success **********************/
                  $client_ip = $this->get_ip_address();

                  $ipAPI_result = @file_get_contents("http://ip-api.com/json/$client_ip");

                  $Ip_Info = json_decode($ipAPI_result);

                  $login_data = 
                  [
                    'employee_id' => $session_array[ 'employee_id' ],
                    'ipaddress'   => $client_ip,
                    'hostname'    => gethostbyaddr($client_ip),
                    'city_region' => @$Ip_Info->city.",".@$Ip_Info->regionName,
                    'country'     => @$Ip_Info->country
                  ];
                  
                  $result = $this->Model_Access->login_record($login_data);
                  
                  if($result) 
                  {
                    $retrieve_tax = $this->Universal_Retrieval->retrieve_tax();
                    $session_array['settings']['tax'] = $retrieve_tax->percentage;
                    $session_array['settings']['vat'] = $retrieve_tax->percentage;
                    $session_array['settings']['getfund'] = $retrieve_tax->getfund;
                    $session_array['settings']['nhil'] = $retrieve_tax->nhil;
                    //print"<pre>";print_r($session_array['settings']['tax']);print"</pre>"; exit;
                    $session_array [ 'LoginID' ]      = $result;
                    $this->session->set_userdata($session_array);
                    $this->Model_Access->revert_login_attempt($username) ;
                    
                    redirect('dashboard');
                  }
                  else
                    $this->session->set_flashdata('error',"Employee Work Data Loading Failed<br>");
                /************************ Recording Login Success **********************/
              }
              
              elseif($data['account_active'] != 1)
              {
                $this->session->set_flashdata('error',"Account Deactivated. Please Contact Administrator");
                redirect('access/login');
              }

              elseif($data['login_attempt'] <= 0)
              {
                $this->session->set_flashdata('error',"LogIn Attempts Exceeded. Please Contact Administrator");
                redirect('access/login');
              }	
            }
            # End of Form Validation Successful	

            # Password Verfication ....... Failed
            else 
            {
              if($data['login_attempt'] > 0)
              {
                $attempt =  $this->Model_Access->decrease_login_attempt($username);

                # Deactivate Account
                if( $attempt == 0 ) 
                {
                  $deactivate_res   = $this->Model_Access->deactivate_account($username,0);
                      
                  if( $deactivate_res )
                    $this->session->set_flashdata('error', "LogIn Attempts Exceeded. Account Deactivated. Please Contact Administrator");
                }
                else
                  $this->session->set_flashdata('attemptleft',$attempt);
              }
              #If Password Failed and Login Attempt = 0
              else
                $this->session->set_flashdata('attemptleft', 0);
              #######################################
              $client_ip = $this->get_ip_address();

              $ipAPI_result = file_get_contents("http://ip-api.com/json/$client_ip");

              $Ip_Info = json_decode($ipAPI_result);

              $login_fail_data = [

                'username'      => $username,
                'password'      => $password,
                'ipaddress'     => $client_ip,
                'hostname'      => gethostbyaddr($client_ip),
                'city_region'   => $Ip_Info->city.",".$Ip_Info->regionName,
                'country'       => $Ip_Info->country
              ];
              
              $this->Model_Access->fakelogin_log($login_fail_data);
              #######################################    
              redirect('access/login');
            }
            # Password Verfication ....... Failed			
          }
          #If Button Not Clicked
          else 
          {
            $this->session->set_flashdata('error','Invalid Username/Password Combination');
            redirect('access/login');
          }
        }
      }
    }					
    #End of Function

  /************************************** Other Func ********************************/

	/************************************** Data Insertion ***************************/
   
        
	/*********************************** Other Functions ****************************************/
    /**
    * Retrieves the best guess of the client's actual IP address.
    * Takes into account numerous HTTP proxy headers due to variations
    * in how different ISPs handle IP addresses in headers between hops.
    */
    public function get_ip_address() 
    {
      // check for shared internet/ISP IP
      if (!empty($_SERVER['HTTP_CLIENT_IP']) && $this->validate_ip($_SERVER['HTTP_CLIENT_IP'])) 
      {
        return $_SERVER['HTTP_CLIENT_IP'];
      }
      // check for IPs passing through proxies
      if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) 
      {
        // check if multiple ips exist in var
        if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
          $iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
          foreach ($iplist as $ip) {
            if ($this->validate_ip($ip))
              return $ip;
          }
        } else {
          if ($this->validate_ip($_SERVER['HTTP_X_FORWARDED_FOR']))
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
      }
      if (!empty($_SERVER['HTTP_X_FORWARDED']) && $this->validate_ip($_SERVER['HTTP_X_FORWARDED']))
        return $_SERVER['HTTP_X_FORWARDED'];
      if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && $this->validate_ip($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
        return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
      if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && $this->validate_ip($_SERVER['HTTP_FORWARDED_FOR']))
        return $_SERVER['HTTP_FORWARDED_FOR'];
      if (!empty($_SERVER['HTTP_FORWARDED']) && $this->validate_ip($_SERVER['HTTP_FORWARDED']))
        return $_SERVER['HTTP_FORWARDED'];
      // return unreliable ip since all else failed
      return $_SERVER['REMOTE_ADDR'];
    }

    /**
     * Ensures an ip address is both a valid IP and does not fall within
     * a private network range.
     */
    public function validate_ip($ip) 
    {
      if (strtolower($ip) === 'unknown')
        return false;
      // generate ipv4 network address
      $ip = ip2long($ip);
      // if the ip is set and not equivalent to 255.255.255.255
      if ($ip !== false && $ip !== -1) {
        // make sure to get unsigned long representation of ip
        // due to discrepancies between 32 and 64 bit OSes and
        // signed numbers (ints default to signed in PHP)
        $ip = sprintf('%u', $ip);
        // do private network range checking
        if ($ip >= 0 && $ip <= 50331647) return false;
        if ($ip >= 167772160 && $ip <= 184549375) return false;
        if ($ip >= 2130706432 && $ip <= 2147483647) return false;
        if ($ip >= 2851995648 && $ip <= 2852061183) return false;
        if ($ip >= 2886729728 && $ip <= 2887778303) return false;
        if ($ip >= 3221225984 && $ip <= 3221226239) return false;
        if ($ip >= 3232235520 && $ip <= 3232301055) return false;
        if ($ip >= 4294967040) return false;
      }
      return true;
    }

  /*********************************** Other Functions ****************************************/

  /************************************** Data Retrieval ***************************/
    /***********************************
      User Roles Retrieve
    ************************************/
      public function roles_retrieve()
      {
        $this->form_validation->set_rules('userid','User ID','required|trim');

        if($this->form_validation->run() === FALSE)
        {
          print "Submission Failed";
        }
        else
        {
          # Loading Model
          $this->load->model('Universal_Retrieval');

          $data['employee_id'] = base64_decode($this->input->post('userid'));
          
          $result = $this->Universal_Retrieval->ret_data_with_s_cond_row('access_roles_priviledges_user','employee_id',$data);

          if($result)
          {
            $roles_exploded = explode('|', $result->roles);

            foreach ($roles_exploded as $roles) 
            {
              if(!empty($roles))
                print "<div class='checkbox'><input type='checkbox' checked readonly class='checkbox1'><label for='checkbox1'> $roles </label></div>";
            }
          }
          else
          {
            print "Roles Retrieval Failed";
          }
        }
      }

      /***********************************
      User Roles Retrieve
      ************************************/
      public function setroles_retrieve()
      {
        $this->form_validation->set_rules('userid','User ID','required|trim');

        if($this->form_validation->run() === FALSE)
        {
          print "Submission Failed";
        }
        else
        {
          # Loading Model
          $this->load->model('Universal_Retrieval');

          $data['employee_id'] = base64_decode($this->input->post('userid'));
          
          $result = $this->Universal_Retrieval->ret_data_with_s_cond_row('access_roles_priviledges_user','employee_id',$data);

          if($result)
          {
            $roles_exploded = explode('|', $result->roles);

            print $result->roles;
          }
          else
          {
            print "Roles Retrieval Failed";
          }
        }
      }

      
  /************************************** Data Retrieval ***************************/
	
}//End of Class
